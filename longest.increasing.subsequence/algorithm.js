export default longestIncreasingSubsequence = (arr) => {
    const sequences = [[arr[0]]];

    for (let i = 1; i < arr.length; i++) {
        for (let s of sequences) {
            if (arr[i] > s[s.length - 1]) s.push(arr[i]);
            
            if (s[s.length - 1] > arr[i]) {
                const newSeq = s.filter(el => el < arr[i]);
                newSeq.push(arr[i]);
                if (
                    newSeq.length === s.length 
                    && newSeq[newSeq.length - 1] < s[s.length - 1]
                ) {
                    const index = sequences.findIndex(el => el.toString() === s.toString());
                    sequences.splice(index, 1);
                }
                sequences.push(newSeq);
            }
        }
    }

    return sequences[sequences.reduce((p, c, i, a) => a[p].length > c.length ? p : i, 0)];
}
